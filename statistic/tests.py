from django.test import TestCase,Client 
from django.urls import resolve
from .views import index,stat1
from update_status.models import Todo
from friends.models import Data_teman
from django.utils import timezone
from django.http import HttpRequest

# Create your tests here.
class StatisticTest(TestCase):

	def test_statistic_url_is_exist(self):
		response = Client().get('/statistic/')
		self.assertEqual(response.status_code, 200)

	def test_statistic_using_index_func(self):
		found = resolve('/statistic/')
		self.assertEqual(found.func, index)

	def test_statistic1_url_is_exist(self):
		response = Client().get('/statistic/stat1/')
		self.assertEqual(response.status_code, 200)

	def test_statistic1_using_index_func(self):
		found = resolve('/statistic/stat1/')
		self.assertEqual(found.func, stat1)

	def testing_nav_and_footer(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('navbar',html_response)
		self.assertIn('Made by',html_response)

	def testing_status_amount(self):
		status_ehe = 'Yoyoyoyooo mannn'
		new_activity = Todo.objects.create(status=status_ehe,created_date=timezone.now())
		jumlahstatus = Todo.objects.all().count();
		coba = Todo.objects.last();
		self.assertEqual(coba.status,status_ehe)
		self.assertEqual(jumlahstatus,1)

	def test_amount_of_friends(self):
		friends = 'Teman Karib'
		urlteman = 'https://temankaribku.herokuapp.com'
		data_teman = Data_teman.objects.create(nama=friends,url=urlteman,tanggal_dibuat=timezone.now())
		jumlahteman = Data_teman.objects.all().count()
		self.assertEqual(jumlahteman,1)