from django.conf.urls import url
from .views import index,stat1
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^stat1',stat1,name='stat1')
]
