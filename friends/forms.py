from django import forms

class Form_teman(forms.Form):
    attrs = {
        'class' : 'input_form',
        'placeholder' : '> Nama',
        'id' : 'nama'
    }
    attrs2 = {
        'class' : 'input_form',
        'placeholder' : '> URL',
        'id' : 'url'
    }
    pesan_error = {
        'required' : 'harap di isi'
    }
    nama = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=attrs), error_messages=pesan_error)
    url  = forms.URLField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=attrs2), error_messages=pesan_error)
