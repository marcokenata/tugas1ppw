from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Data_teman
from .forms import Form_teman

# Create your tests here.

class temanUnitTest(TestCase):

    def test_teman_url_is_exist(self):
        response = Client().get('/friends/')
        self.assertEqual(response.status_code, 200)

    def test_teman_using_index_func(self):
        found = resolve('/friends/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_teman_data(self):
        Data_teman_baru = Data_teman.objects.create(nama='nama-test', url='http://blabla.com')
        counting_all_available_data_teman = Data_teman.objects.all().count()
        self. assertEqual(counting_all_available_data_teman, 1)

    def test_form_teman_input_has_placeholder_and_css_classes(self):
        form = Form_teman()
        self.assertIn('class="input_form"', form.as_p())
        self.assertIn('placeholder="&gt; Nama"', form.as_p())
        self.assertIn('placeholder="&gt; URL"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Form_teman(data={'nama': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['url'],
            ["harap di isi"]
        )

    def test_teman_post_success_and_render_the_result(self):
        tes = 'anonymous'
        response_post = Client().post('/friends/tambahkan-teman/', {'nama':tes, 'url':'http://blabla.com'})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/friends/')
        html_response = response.content.decode('utf8')
        self.assertIn(tes, html_response)

    def test_lab5_post_error_and_render_the_result(self):
        test = 'anonymous'
        response_post = Client().post('/friends/tambahkan-teman/', {'nama': '', 'url': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/friends/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
