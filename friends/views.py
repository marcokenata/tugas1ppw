from django.shortcuts import render, redirect
from .forms import Form_teman
from .models import Data_teman

# Create your views here.
kam = {}
html = 'index.html',

def index(request):
    kam['form_teman'] = Form_teman
    tampilkan_daftar(request)
    return render(request, html, kam)

def tambah_POST(request):
    #if  request.method == 'POST':       coba aja komen ini. nanti dia multivaluedictkey error
    form = Form_teman(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        kam['nama'] = request.POST['nama']
        kam['url'] = request.POST['url']
        data = Data_teman(nama=kam['nama'], url=kam['url'])
        data.save()
        return redirect('/friends/')
    else:
        return redirect('/friends/')

def tampilkan_daftar(request):
    data = Data_teman.objects.all()
    kam['data'] = data
