from django.conf.urls import url
from .views import index, tambah_POST

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^tambahkan-teman', tambah_POST, name='tambah_POST')
]
