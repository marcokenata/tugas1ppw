from django.db import models

# Create your models here.

class Data_teman(models.Model):
    nama = models.CharField(max_length=27)
    url = models.URLField(max_length=27)
    tanggal_dibuat = models.DateTimeField(auto_now_add=True)
