from django.shortcuts import render
from datetime import datetime, date

# Create your views here.
profile_name = 'Lee Jong-suk' #TODO implement your name here
birth_date = date(1989,9,14) #TODO implement your birthday
birthdate = birth_date.strftime('%d %B %Y')
gender = 'Male' #TODO implement your gender
email = 'jongsuklee@gmail.com' #TODO implement your email
desc_profile = "A desperate ninja"
#TODO implement your expertise minimal 3
expert = ["Actor", "Model", "Oppa"]

response = {}
def index(request):
    response['author'] = "Mona Natasha"
    response['Name'] = profile_name
    response['Birthday'] = birthdate
    response['Gender'] = gender
    response['expertise'] = expert
    response['Email'] = email
    response['Description'] = desc_profile
    return render(request, 'user_profile/user_profile.html', response)
