from django.db import models

# Create your models here.
class Person(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField()
    gender = models.TextField()
    description = models.TextField()
    birthday = models.DateTimeField()
