from django.conf.urls import url
from .views import index, add_status, postdelete

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', add_status, name='add_status'),
    url(r'^postdelete/(?P<id_status>\d+)/$', postdelete, name='postdelete'),
]
