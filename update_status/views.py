from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo

# Create your views here.
response = {}
def index(request):
    response['author'] = "Lee Jong-suk" #TODO Implement yourname
    todo = Todo.objects.order_by('-id')
    response['todo'] = todo
    html = 'update.html'
    response['todo_form'] = Todo_Form
    return render(request, html, response)

def add_status(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        todo = Todo(status=response['status'])
        todo.save()
        return HttpResponseRedirect('/update_status/')
    else:
        return HttpResponseRedirect('/update_status/')

def postdelete(request,id_status):
    status = Todo.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/update_status/')
