from django.db import models
from django.db import models

class Todo(models.Model):
    status = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
