"""sosialmedia URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
import update_status.urls as update_status
import statistic.urls as statistic
import user_profile.urls as user_profile
import update_status.urls as update_status
import friends.urls as friends
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^update_status/', include(update_status,namespace='update_status')),
	url(r'^statistic/', include(statistic,namespace='statistic')),
	url(r'^user_profile/', include(user_profile,namespace='user_profile')),
	url(r'^$', RedirectView.as_view(permanent=True,   url='/update_status/')),
    url(r'^friends/', include(friends, namespace='friends'))
]
